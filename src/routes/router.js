const profileRouter = require('./profile');
const home = require('./home');


function route(app){
    app.use('/profile',profileRouter);
    app.use('/',home)
}

module.exports = route;