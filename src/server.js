const db = require('./config/database/database');
const app = require('./app');
const port = 3000;
db.connect();

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })