const Joi = require('joi');

const registerValidation = data =>{
const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(6)
        .max(30)
        .required(),
    email: Joi.string().required()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    password_confirmation: Joi.ref('password'),

})

    return schema.validate(data)
};

module.exports.registerValidation = registerValidation;