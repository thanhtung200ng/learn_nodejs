// // models/user.model.js
// // load những thư viện chúng ta cần
// var mongoose = require('mongoose');
// var bcrypt = require('bcrypt-nodejs');
// // định nghĩ cấu trúc user model
// var Schema = mongoose.Schema;
// var User = new Schema({
//     email: {type: String, required: true},
//     password: {type: String, required: true}
// });
// User.methods.encryptPassword= function(password){
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(5),null);
// };
// User.methods.validPassword = function(password){
//     return bcrypt.compareSync(password, this.password);
// };
// module.exports = mongoose.model('User', User);

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
 username:{
     type:String,
     required:true,
     min:6,
     max:255
 },
 email:{
    type:String,
    required:true,
    min:6,
    max:255
},
password:{
    type:String,
    required:true,
    min:6,
    max:255
},
create_at:{
    type:Date,
    default:Date.now
}
});

module.exports = mongoose.model('User', userSchema);