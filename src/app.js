var morgan = require('morgan');
const express = require('express');
const bodyParser = require("body-parser");
const passport = require('passport');
// const passportJWT = require('passport-jwt');
// const ExtractJwt = passportJWT.ExtractJwt;
// var JwtStrategy = passportJWT.Strategy;
// var jwtOptions = {};
// var jwt = require('jsonwebtoken');


const flash = require('connect-flash');
const session = require('express-session')
const cors = require("cors");  
const path = require('path');
const route =require('./routes/router')
const handlebars = require('express-handlebars');
const db = require('./config/database/database');
const app = express();

const authRoute =require('./routes/auth');

require('./config/passport');
//auth
  

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(session({
  secret: 'adsa897adsa98bs',
  resave: false,
  saveUninitialized: false,
  }))
  app.use(flash());
  app.use(passport.initialize())
  app.use(passport.session());
  

//router
app.use('/user',authRoute)
route(app);


// var corsOptions = {
//   origin: `http://localhost:${port}`
// };



//handlebars
app.engine('.hbs', handlebars({extname: '.hbs'}));//extname custom đuôi handlebars
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources','views'));
// app.get('/', function (req, res) {
 
//   res.render('register', { layout: 'auth' });
// });

//morgan
app.use(morgan('combined'));
//static file
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'resources')));

module.exports = app;