const express = require('express');
const router = express.Router();
const homeController = require('../app/controller/client/HomeController');

router.use('/home', homeController.index);

module.exports = router;