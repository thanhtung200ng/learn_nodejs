var User = require('../models/user.model');
var bcrypt =require('bcryptjs');

const {registerValidation} = require('../validation')

// const schema = {
//     username: Joi.string()
//         .alphanum()
//         .min(3)
//         .max(30)
//         .required(),
//     email: Joi.string().required()
//         .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
//     password: Joi.string()
//         .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
//     password_confirmation: Joi.ref('password'),
//   }

class AuthController{
    
   
    async register(req,res) { 
        
        const {error} = await registerValidation(req.body);
        if(error){
        res.status(400).send(error.details[0].message)}
        else{
            console.log('dung cmr')
        }
       
      
       

        const userNameExist = User.findOne({username:req.body.username});
        //hash password
        const salt = await bcrypt.genSalt(10);
        const hasPassword = await bcrypt.hash(req.body.password, salt);

        //save
        const user = new User({
            username:req.body.username,
            email:req.body.email,
            password:hasPassword
          })
          try {
             const saveUser = await user.save(); 
             res.send(saveUser);
          } catch (error) {
              res.status(400).send(error);
          }


        }
}
module.exports = new AuthController;